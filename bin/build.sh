#!/bin/bash -ex

: ${SRC_BOX_NAME:=rocky8-base}
: ${CI_PIPELINE_ID:="0"}
: ${CI_PROJECT_NAME:="rocky8/container-runtimes"}
: ${SCRATCH_DIR:="/scratch"}
: ${PARENT_PIPELINE_ID:="0"}

SRC_PATH="${SCRATCH_DIR}/${SRC_BOX_NAME}/${PARENT_PIPELINE_ID}/package.box"
echo ">>> SRC_PATH: ${SRC_PATH}"

if [[ "${PACKER_FORCE}" == "true" ]]; then
    PACKER_OPTS="${PACKER_OPTS} -force"
fi

packer build ${PACKER_OPTS} -var "scratch_dir=${SCRATCH_DIR}" \
             -var "ci_pipeline_id=${CI_PIPELINE_ID}" \
             -var "ci_project_name=${CI_PROJECT_NAME}" \
             -var "source_path=${SRC_PATH}" packer.json